package message

import (
	"io"
	"net"
)

type Service interface {
	io.Closer
	Receiver
	Sender
	GetAddr() net.Addr
}
