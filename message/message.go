package message

import "gitlab.com/remeex/godaser/serializable"

type Message []byte

type messageReader struct {
	data   []byte
	offset int
}

func (m *messageReader) Read(p []byte) (n int, err error) {
	l1 := len(m.data) - m.offset
	l2 := len(p)
	if l1 >= l2 {
		copy(p, m.data[m.offset:m.offset+l2])
		m.offset += l2
		return l2, nil
	} else {
		copy(p, m.data[m.offset:m.offset+l1])
		m.offset = len(m.data)
		return l1, nil
	}
}

func (m *Message) ToSerializable(s serializable.Serializable) error {
	mr := &messageReader{data: *m}
	return s.Deserialize(serializable.NewDeserializer(mr))
}

type messageWriter struct {
	data []byte
}

func (m *messageWriter) Write(p []byte) (n int, err error) {
	m.data = append(m.data, p...)
	return len(p), nil
}

func FromSerializable(s serializable.Serializable) (Message, error) {
	mw := &messageWriter{data: make([]byte, 0, 16)}
	err := s.Serialize(serializable.NewSerializer(mw))
	return mw.data, err
}
