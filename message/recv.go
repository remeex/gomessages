package message

type Receiver interface {
	ReceiveMessage() (Message, error)
}
