package message

import (
	"io"
	"net"
)

type ServiceAcceptor interface {
	io.Closer
	Accept() (Service, error)
	GetAddr() net.Addr
}
