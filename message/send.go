package message

type Sender interface {
	SendMessage(m Message) error
}
