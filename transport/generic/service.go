package generic

import (
	"errors"
	"gitlab.com/remeex/godaser/serializable"
	"gitlab.com/remeex/gomessages/message"
	"io"
	"net"
)

type genericMessageService struct {
	c io.Closer
	a net.Addr
	s *serializable.Serializer
	d *serializable.Deserializer
}

func (gms *genericMessageService) readMessage() (message.Message, error) {
	if l, err := gms.d.ReadInt(); err != nil {
		return nil, err
	} else if b, err := gms.d.ReadNewBytes(l); err != nil {
		return nil, err
	} else {
		return b, nil
	}
}

func (gms *genericMessageService) writeMessage(m message.Message) error {
	if err := gms.s.WriteInt(len(m)); err != nil {
		return err
	} else {
		return gms.s.WriteBytes(m)
	}
}

func (gms *genericMessageService) Close() error {
	if gms.c != nil {
		return gms.c.Close()
	}
	return nil
}

func (gms *genericMessageService) ReceiveMessage() (message.Message, error) {
	if gms.d == nil {
		return nil, errors.New("no input reader")
	}
	return gms.readMessage()
}

func (gms *genericMessageService) SendMessage(m message.Message) error {
	if gms.s == nil {
		return errors.New("no output writer")
	}
	return gms.writeMessage(m)
}

func (gms *genericMessageService) GetAddr() net.Addr {
	return gms.a
}

func New(w io.Writer, r io.Reader, c io.Closer, a net.Addr) message.Service {
	return &genericMessageService{
		c: c,
		a: a,
		s: serializable.NewSerializer(w),
		d: serializable.NewDeserializer(r),
	}
}
