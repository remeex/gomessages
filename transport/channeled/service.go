package channeled

import (
	"errors"
	"gitlab.com/remeex/godaser/types"
	"gitlab.com/remeex/gomessages/message"
	"net"
)

type msgChannel = types.DUInt8

type msg struct {
	c *msgChannel
	m message.Message
}

type channeledService struct {
	channels []chan *msg
	base     message.Service
}

func (cs *channeledService) Close() error {
	for _, it := range cs.channels {
		close(it)
	}
	return cs.base.Close()
}

func (cs *channeledService) ReceiveMessage(channel uint8) (message.Message, error) {
AGAIN:
	select {
	case m := <-cs.channels[channel]:
		return m.m, nil
	default:
	}

	raw, err := cs.base.ReceiveMessage()
	if err != nil {
		return nil, err
	}
	c := new(msgChannel)
	err = raw.ToSerializable(c)
	if err != nil {
		return nil, err
	}
	raw, err = cs.base.ReceiveMessage()
	if err != nil {
		return nil, err
	}
	if c.Value == channel {
		return raw, nil
	}
	m := &msg{
		c: c,
		m: raw,
	}
	cs.channels[c.Value] <- m
	goto AGAIN
}

func (cs *channeledService) SendMessage(channel uint8, m message.Message) error {
	mc, err := message.FromSerializable(&msgChannel{Value: channel})
	if err == nil {
		err = cs.base.SendMessage(mc)
	}
	if err == nil {
		err = cs.base.SendMessage(m)
	}
	return err
}

func (cs *channeledService) GetAddr() net.Addr {
	return cs.base.GetAddr()
}

func New(base message.Service, channelCount uint8, bufLen int) (cs *channeledService, err error) {
	defer func() {
		if re := recover(); re != nil {
			if e, ok := re.(error); ok {
				if err == nil {
					err = e
				} else {
					err = errors.New(err.Error() + "\n" + e.Error())
				}
			} else {
				panic(re)
			}
		}
	}()

	if channelCount < 2 {
		return nil, errors.New("channelCount should be >= 2")
	}
	cs = &channeledService{
		channels: make([]chan *msg, channelCount),
		base:     base,
	}
	if bufLen < 0 {
		for i := range cs.channels {
			cs.channels[i] = make(chan *msg)
		}
	} else {
		for i := range cs.channels {
			cs.channels[i] = make(chan *msg, bufLen)
		}
	}
	return
}
