package channeled

import (
	"gitlab.com/remeex/godaser/types"
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/gomessages/transport/tcp"
	"testing"
)

func startServer() (message.ServiceAcceptor, error) {
	return tcp.NewAcceptor(8080)
}

func connectToServer() (message.Service, error) {
	return tcp.NewClient("127.0.0.1", 8080)
}

func TestChannel2(t *testing.T) {
	acc, err := startServer()
	if err != nil {
		t.Log("cannot start server on port 8080>", err.Error())
		return
	}

	go func() {
		client, err := acc.Accept()
		if err != nil {
			t.Fatal("cannot accept client on server 127.0.0.1:8080>", err.Error())
			return
		}

		msg1 := &types.DInt16{Value: 256}
		msg2 := &types.DString{Value: "Hello"}
		msg3 := &types.DString{Value: "MyPassword"}

		toClient1, err1 := message.FromSerializable(msg1)
		toClient2, err2 := message.FromSerializable(msg2)
		toClient3, err3 := message.FromSerializable(msg3)

		if err1 != nil {
			t.Log("err1>", err1.Error())
		}
		if err2 != nil {
			t.Log("err2>", err2.Error())
		}
		if err3 != nil {
			t.Log("err3>", err3.Error())
		}
		if err1 != nil || err2 != nil || err3 != nil {
			t.Fatal("serializing errors")
			return
		}

		chan2, err := New(client, 2, 4)
		if err != nil {
			client.Close()
			t.Fatal("cannot create channeled service>", err.Error())
			return
		}

		err1 = chan2.SendMessage(0, toClient1)
		err2 = chan2.SendMessage(1, toClient2)
		err3 = chan2.SendMessage(0, toClient3)

		if err1 != nil {
			t.Log("err1>", err1.Error())
		}
		if err2 != nil {
			t.Log("err2>", err2.Error())
		}
		if err3 != nil {
			t.Log("err3>", err3.Error())
		}
		if err1 != nil || err2 != nil || err3 != nil {
			chan2.Close()
			t.Fatal("channel errors")
			return
		}

		chan2.Close()
	}()

	client, err := connectToServer()
	if err != nil {
		t.Fatal("cannot connect to server 127.0.0.1:8080>", err.Error())
		return
	}

	chan2, err := New(client, 2, 4)
	if err != nil {
		client.Close()
		acc.Close()
		t.Fatal("cannot create channeled service>", err.Error())
		return
	}

	fromServer1, err1 := chan2.ReceiveMessage(1)
	fromServer2, err2 := chan2.ReceiveMessage(0)
	fromServer3, err3 := chan2.ReceiveMessage(0)

	if err1 != nil {
		t.Log("err1>", err1.Error())
	}
	if err2 != nil {
		t.Log("err2>", err2.Error())
	}
	if err3 != nil {
		t.Log("err3>", err3.Error())
	}
	if err1 != nil || err2 != nil || err3 != nil {
		chan2.Close()
		acc.Close()
		t.Fatal("channel errors")
		return
	}

	msg1 := &types.DString{Value: "none"}
	msg2 := &types.DInt16{Value: 0}
	msg3 := &types.DString{Value: "none"}

	err1 = fromServer1.ToSerializable(msg1)
	err2 = fromServer2.ToSerializable(msg2)
	err3 = fromServer3.ToSerializable(msg3)

	if err1 != nil {
		t.Log("err1>", err1.Error())
	}
	if err2 != nil {
		t.Log("err2>", err2.Error())
	}
	if err3 != nil {
		t.Log("err3>", err3.Error())
	}
	if err1 != nil || err2 != nil || err3 != nil {
		chan2.Close()
		acc.Close()
		t.Fatal("deserializing errors")
		return
	}

	if msg1.Value != "Hello" {
		chan2.Close()
		acc.Close()
		t.Fatal("message[0] != \"Hello\", actual \"" + msg1.Value + "\"")
		return
	}

	if msg2.Value != 256 {
		chan2.Close()
		acc.Close()
		t.Fatal("message[1] != 256, actual", msg2.Value)
		return
	}

	if msg3.Value != "MyPassword" {
		chan2.Close()
		acc.Close()
		t.Fatal("message[2] != \"MyPassword\", actual \"" + msg3.Value + "\"")
		return
	}

	chan2.Close()
	acc.Close()
}
