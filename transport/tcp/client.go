package tcp

import (
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/gomessages/transport/generic"
	"net"
	"strconv"
)

func NewClient(ip string, port int) (message.Service, error) {
	if client, err := net.Dial("tcp", ip+":"+strconv.Itoa(port)); err != nil {
		return nil, err
	} else {
		return generic.New(client, client, client, client.RemoteAddr()), nil
	}
}
