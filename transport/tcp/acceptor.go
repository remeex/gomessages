package tcp

import (
	"gitlab.com/remeex/gomessages/message"
	"gitlab.com/remeex/gomessages/transport/generic"
	"net"
	"strconv"
)

type tcpAcceptor struct {
	l net.Listener
}

func (tcp *tcpAcceptor) Close() error {
	return tcp.l.Close()
}

func (tcp *tcpAcceptor) Accept() (message.Service, error) {
	if client, err := tcp.l.Accept(); err != nil {
		return nil, err
	} else {
		return generic.New(client, client, client, client.RemoteAddr()), nil
	}
}

func (tcp *tcpAcceptor) GetAddr() net.Addr {
	return tcp.l.Addr()
}

func NewAcceptor(port int) (message.ServiceAcceptor, error) {
	if l, err := net.Listen("tcp", ":"+strconv.Itoa(port)); err != nil {
		return nil, err
	} else {
		return &tcpAcceptor{l: l}, nil
	}
}
